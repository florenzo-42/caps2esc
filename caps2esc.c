#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <unistd.h>
#include <linux/input.h>

/*
 * For EV_KEY event value means the following:
 * 0 -> key release
 * 1 -> key press
 * 2 -> key autorepeat
 */

const struct input_event
syn             = {.type = EV_SYN, .code = SYN_REPORT,    .value = 0};

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

void write_key_press(int key) {
	struct input_event target = { .type = EV_KEY, .code = key, .value = 1, };
	write_event(&target);
}

void write_key_release(int key) {
	struct input_event target = { .type = EV_KEY, .code = key, .value = 0, };
	write_event(&target);
}

bool is_key(int key, struct input_event *event) {
	return event->code == key;
}

bool is_pressed(int key, struct input_event *event) {
	return is_key(key, event) && event->value == 1;
}

bool is_repeated(int key, struct input_event *event) {
	return is_key(key, event) && event->value == 2;
}

bool is_released(int key, struct input_event *event) {
	return is_key(key, event) && event->value == 0;
}

struct dual_function_rule {
	int catch;
	int pulse;
	int modifier;
	// these should default to false
	bool catch_is_down;
	bool modifying;
};

// clang-format off
struct dual_function_rule dual_function_keys[] = {
	{ .catch = KEY_CAPSLOCK,  .pulse = KEY_ESC,       .modifier = KEY_LEFTCTRL,  },
	{ .catch = KEY_SPACE,     .pulse = KEY_SPACE,     .modifier = KEY_LEFTSHIFT, },
	{ .catch = KEY_ENTER,     .pulse = KEY_ENTER,     .modifier = KEY_LEFTMETA,  },
	{ .catch = KEY_BACKSPACE, .pulse = KEY_BACKSPACE, .modifier = KEY_LEFTALT,   },
};
// clang-format on

bool dual_function(struct dual_function_rule *rule, struct input_event *input) {
	if (rule->catch_is_down) {
		if (is_pressed(rule->catch, input)
				|| is_repeated(rule->catch, input)) {
			return true;
		}

		if (is_released(rule->catch, input)) {
			rule->catch_is_down = false;
			if (rule->modifying) {
				rule->modifying = false;
				write_key_release(rule->modifier);
				return true;
			}
			write_key_press(rule->pulse);
			write_event(&syn);
			usleep(20000);
			write_key_release(rule->pulse);
			return true;
		}

		if (!rule->modifying && !is_key(rule->catch, input)) {
			rule->modifying = true;
			write_key_press(rule->modifier);
			write_event(&syn);
			usleep(20000);
		}
	} else if (is_pressed(rule->catch, input)) {
		rule->catch_is_down = true;
		return true;
	}
	return false;
}

int main(void) {
    struct input_event input;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN)
            continue;

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

		if (dual_function(dual_function_keys, &input)
				|| dual_function(dual_function_keys + 1, &input)
				|| dual_function(dual_function_keys + 2, &input)
				|| dual_function(dual_function_keys + 3, &input)) {
			continue;
		}

        write_event(&input);
    }
}
